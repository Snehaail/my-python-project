# Assignment 1: Git Branching and Workflow

## Objective
Implement and manage a branching strategy in Git.

## Repository URL
[GitLab Repository](https://gitlab.com/Snehaail/git-branching)

## Instructions
- Feature Branch: Created a branch named `feature/new-feature`.
- Release Branch: Created a branch named `release/v1.0.0`.
- Merged `feature/new-feature` into `release/v1.0.0`.
- Tagged the release branch with `v1.0.0`.

## Branches
- `main`
- `feature/new-feature`
- `release/v1.0.0`

# Assignment 2: Dockerizing a Python Application

## Objective
Containerize a Flask or FastAPI application using Docker and Docker Compose.

## Repository URL
[GitLab Repository](https://gitlab.com/Snehaail/dockerization)

## Files
- `Dockerfile`: Docker configuration.
- `docker-compose.yml`: Docker Compose configuration.
- `app.py`: Application code.
- `requirements.txt`: Dependencies.

## Instructions
- Build Docker Image: `docker build -t my-python-project .`
- Run Docker Container: `docker run -p 5003:5000 my-python-project`
- Use Docker Compose: `docker-compose up --build`

##Output
-Application runs on http://98.70.32.170:5000/

# Assignment 3: Free Style Job with Jenkins

## Objective
Set up a free style job in Jenkins to take a git pull of dockerrise application created in assignment 2.

## Repository URL
[GitLab Repository](https://gitlab.com/Snehaail/jenkins)

## Jenkins Setup
- Jenkins URL: [Jenkins](http://98.70.32.170:8080/)
- Configured Jenkins to connect to the Git repository.
- Created a Freestyle Job to pull, build, and launch the Dockerized application.

## Build Steps in Jenkins
- Pull the latest code from GitLab.
- Stop existing containers: `docker-compose down`
- Build and run the application: `docker-compose up -d --build`

## Verification
- Application runs on (http://98.70.32.170:5003)
- Changes in the repository reflect in the running application.

